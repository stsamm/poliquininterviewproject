﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PoliquinInterviewProject.Data;
using PoliquinInterviewProject.Models;

namespace PoliquinInterviewProject.Tests
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void EnsureAreaTranslation()
        {
            // Arkansas -> "area" : "137733SKM",
            var arkansas = RESTfulAreaTranslator.Translate("137733SKM");

            // California -> "area" : "423968SKM",
            var california = RESTfulAreaTranslator.Translate("423968SKM");

            Assert.AreEqual(137733, arkansas);
            Assert.AreEqual(423968, california);
        }

        [TestMethod]
        public void EnsureAreaTranslationFails()
        {
            var badValue1 = RESTfulAreaTranslator.Translate(null);
            var badValue2 = RESTfulAreaTranslator.Translate("");
            var badValue3 = RESTfulAreaTranslator.Translate("-.SKM");

            Assert.AreEqual(-1, badValue1);
            Assert.AreEqual(-1, badValue2);
            Assert.AreEqual(-1, badValue3);
        }

        [TestMethod]
        public void EnsureStateWithMostSimilarArea()
        {
            var list = new ModelState[]
            {
                new ModelState
                {
                    Name = "State 1",
                    Area = 6,
                },
                new ModelState
                {
                    Name = "State 2",
                    Area = 10,
                },
            };

            var stateTest1 = new ModelState
            {
                Name = "Test",
                Area = 1,
            };
            var stateTest2 = new ModelState
            {
                Name = "Test",
                Area = 7,
            };

            var result1 = StateUtility.GetStateWithMostSimilarArea(stateTest1, list);
            var result2 = StateUtility.GetStateWithMostSimilarArea(stateTest2, list);

            Assert.AreEqual("State 1", result1.Name);
            Assert.AreEqual("State 1", result2.Name);
        }
    }
}
