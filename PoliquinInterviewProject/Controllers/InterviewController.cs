﻿using PoliquinInterviewProject.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PoliquinInterviewProject.Controllers
{
    public class InterviewController : Controller
    {
        private const string DEFAULT_MESSAGE = @"You found a bug! Can you fix it? Can you explain what caused the bug and how to avoid it in the future?";

        /// <summary>
        /// Returns the view for the landing page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Returns a view for selecting a state.
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> SelectState()
        {
            var adapter = Factory.GetDefault();
            var list = await adapter.GetStatesAsync();
            TempData["list"] = list;
            return View(list);
        }

        /// <summary>
        /// Returns a view displaying statistic for a selected state.
        /// </summary>
        /// <param name="stateName">The name of the state for which to display the statistics.</param>
        /// <returns></returns>
        public ActionResult DisplayStateStatistics(string stateName)
        {
            ViewBag.Message = DEFAULT_MESSAGE;

            var list = (IEnumerable<Models.ModelState>) TempData["list"];
            if (list !=null)
            {
                var state = list.Where(i => string.Compare(i.Name, stateName) == 0).FirstOrDefault();
                if (state != null)
                {                    
                    ViewBag.Message = string.Format("The largest city in {0} is {1}. The state has an area of {2:N0} sq/km.", state.Name, state.LargestCity, state.Area);
                }
            }

            return View();
        }
    }
}