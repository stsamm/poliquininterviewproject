﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PoliquinInterviewProject.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PoliquinInterviewProject.Data
{
    public class RESTfulDataAdapter : IDataAdapter
    {
        public Uri Uri { get; private set; }

        public RESTfulDataAdapter(string url)
        {
            Uri = new Uri(url);
        }

        /// <summary>
        /// Gets a list of states from the underlying web service.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<ModelState>> GetStatesAsync()
        {
            List<ModelState> result = default(List<ModelState>);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Uri.GetLeftPart(UriPartial.Authority));
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.Timeout = new TimeSpan(0, 0, 0, 10);

                HttpResponseMessage response = await client.GetAsync(Uri.PathAndQuery)
                    // don't resume on the context, use a threadpool thread
                    .ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var rootObject = JsonConvert.DeserializeObject<RootObject>(content);

                    if (rootObject != null)
                    {
                        if(rootObject.RestResponse!= null)
                        {
                            if (rootObject.RestResponse.result != null)
                            {
                                var query = rootObject.RestResponse.result.Select(i => new ModelState
                                {
                                    Name = i.name,
                                    LargestCity = i.largest_city,
                                    Area = RESTfulAreaTranslator.Translate(i.area),
                                });
                                result = query.ToList();
                            }
                        }
                    }
                }
            }

            return result;
        }
    }
}