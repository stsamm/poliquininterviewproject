﻿using PoliquinInterviewProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoliquinInterviewProject.Data
{
    public interface IDataAdapter
    {
        /// <summary>
        /// Returns a list of states.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<ModelState>> GetStatesAsync();
    }
}
